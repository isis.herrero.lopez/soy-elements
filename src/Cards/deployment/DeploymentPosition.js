import React from "react";
import "./DeploymentPosition.css";

import IndividualCardBox from "../individualCards/IndividualCardBox";

export default function DeploymentPosition(props) {
    const soldier_cards = 4;
    const leader_cards = 2;
    const terrain_cards = 2;

    let soldiers_in_position = [];
    for (let i = 0; i < soldier_cards; i++) {
        /*in the future, the key will be:
        props.faction + props.cardNumber */
        soldiers_in_position.push(<IndividualCardBox 
            key={"soldier" + (i + 1)} 
            visible= {props.visible} />);
    }
    const left_soldiers_position = 12.8 + ((soldier_cards - 1) * 5);
    const left_style = "calc(calc(100% - " + left_soldiers_position + "em) / 2)"

    let leaders_in_position = [];
    for (let i = 0; i < leader_cards; i++) {
        /*in the future, the key will be:
        props.faction + props.cardNumber */
        leaders_in_position.push(<IndividualCardBox 
            key={"soldier" + (i + 1)} 
            visible= {props.visible} />);
    }

    let terrain_in_position = [];
    for (let i = 0; i < terrain_cards; i++) {
        /*in the future, the key will be:
        props.faction + props.cardNumber */
        terrain_in_position.push(<IndividualCardBox 
            key={"soldier" + (i + 1)} 
            visible= {props.visible} />);
    }

    return(
        <div className="deployment_position_supracontainer">
            <div className="soldiers_position" style={{left: left_style}}>
                {soldiers_in_position}
            </div>
            <div className="leaders_position">
                {leaders_in_position}
            </div>
            <div className="terrain_position">
                {terrain_in_position}
            </div>
        </div>
    )
}