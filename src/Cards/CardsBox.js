import React from "react";
import "./CardsBox.css";

import IndividualCardBox from "./individualCards/IndividualCardBox";
import DeckBox from "./deck/DeckBox";
import HandBox from "./hand/HandBox";
import DeploymentPosition from "./deployment/DeploymentPosition";
import ShufflingDeckBox from "./deck/ShufflingDeckBox";
import LandingDeckBox from "./deck/LandingDeckBox";

export default function CardsBox(props) {
    let cards_box_content;
    if (props.action === "showCard") {
        cards_box_content = <IndividualCardBox visible="back" />;
    } else if (props.action === "showDeck") {
        cards_box_content = <DeckBox visible="back" size={110} />
    } else if (props.action === "showHand") {
        cards_box_content = <HandBox visible="back" />
    } else if (props.action === "showInitialHand") {
        cards_box_content = <HandBox visible="back" initial={true} />
    } else if (props.action === "showDeploymentPosition") {
        cards_box_content = <DeploymentPosition visible="front" />
    } else if (props.action === "cardTurning") {
        cards_box_content = <IndividualCardBox visible="back" turning={true} alone={true} />;
    } else if (props.action === "shuffling") {
        cards_box_content = <ShufflingDeckBox visible="back" />
    } else if (props.action === "landing") {
        cards_box_content = <LandingDeckBox visible="back" />
    } else if (props.action === "handTurning") {
        cards_box_content = <HandBox visible="back" turning={true} />
    } else if (props.action === "initialHandTurning") {
        cards_box_content = <HandBox visible="back" turning={true} initial={true}/>
    } else if (props.action === "cardSelect") {
        cards_box_content = <HandBox visible="front" selecting={true} />
    }

    return (
        <div className="cards_box_supracontainer">
            {cards_box_content}
        </div>
    )
}