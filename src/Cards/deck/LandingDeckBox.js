import React, { useState, useEffect } from "react";
import "./LandingDeckBox.css";

import DeckBox from "./DeckBox";

export default function LandingDeckBox(props) {
    const final = 110;
    const [finalDeck, setFinalDeck] = useState([]);
    const [gralClass, setGralClass] = useState("landing_supracontainer on_wait");

    useEffect(() => {
        if (finalDeck.length === 0) {
            const new_final_deck = [];
            for (let i = 0; i < final; i++) {
                let delay = (i * 0.02).toFixed(2);
                let bottom = (i / 2) + 60;
                let realBottom = i /2;
                const card = {
                    i,
                    delay: delay + "s",
                    bottom,
                    realBottom,
                }
                new_final_deck.push(card);
            }
            setFinalDeck(new_final_deck);
        }
    }, [finalDeck.length]);

    const handleLanding = () => {
        setGralClass("landing_supracontainer");
    }

    return (
        <div className={gralClass}>
            <div>
                <DeckBox size={final} visible={props.visible} info={finalDeck} action={"landing"} handleLanding={() => handleLanding()} />
            </div>
        </div>
    )
}