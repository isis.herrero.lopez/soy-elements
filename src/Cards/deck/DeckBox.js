import React , { useState, useEffect } from "react";
import "./DeckBox.css";

import IndividualCardBox from "../individualCards/IndividualCardBox";
import LoadingCard from "./LoadingCard";

export default function DeckBox(props) {
    const deck_size = props.size;
    let deck_content = [];
    const [deckDone, setDeckDone] = useState(false);
    let deckCount = 0;

    const handleDeck = () => {
        if (props.action === "shuffling") {
            if (deckCount < (deck_size - 1)) {
                deckCount++;
            } else {
                setDeckDone(true);
            }
        } else if (props.action === "landing") {
            if (deckCount < (deck_size - 1)) {
                deckCount++;
            } else {
                setDeckDone(true);
            }
        }
    }

    useEffect(() => {
        if (deckDone === true && props.action === "landing") {
            props.handleLanding();
        }
    });

    for (let i = 0; i < deck_size; i++) {
        if (props.info === undefined) {
            const bottom = i / 2;
            const style = {bottom: bottom};
            deck_content.push(<IndividualCardBox 
                key={"deck" + (i + 1)}
                id={"deck" + (i + 1)}
                style={style}
                visible={props.visible}
            />);
        } else {
            if (props.info.length > 0 && props.action === "shuffling") {
                const realBottom = i / 2;
                deck_content.push(<IndividualCardBox 
                    key={"deck" + (i + 1)}
                    id={"deck" + (i + 1)}
                    visible={props.visible}
                    className={(props.info[i] !== undefined) ? props.info[i].className : "" }
                    delay={((props.info[i] !== undefined) ? props.info[i].delay : "" )}
                    bottom={((props.info[i] !== undefined) ? props.info[i].bottom : "" )}
                    realBottom={realBottom}
                    deckDone={deckDone}
                    handleDeck={() => handleDeck()}
                    action={"shuffling"}
                />);
            } else if (props.info.length > 0 && props.action === "landing") {
                deck_content.push(<IndividualCardBox 
                    key={"deck" + (i + 1)}
                    id={"deck" + (i + 1)}
                    visible={props.visible}
                    delay={((props.info[i] !== undefined) ? props.info[i].delay : "" )}
                    bottom={((props.info[i] !== undefined) ? props.info[i].bottom : "" )}
                    realBottom={((props.info[i] !== undefined) ? props.info[i].realBottom : "" )}
                    deckDone={deckDone}
                    action={"landing"}
                    handleDeck={() => handleDeck()}
                />);
            }
        }
    }

    if (props.info !== undefined) {
        if (props.action === "shuffling") {
            return (
                <div className="deck_box_supracontainer">
                    <div className={deckDone === false ? "card_container loading_deck": "card_container loading_deck_hidden"}>
                        <LoadingCard style={{display: "flex"}} />
                    </div>
                    {deck_content}
                </div>
            );
        } else if (props.action === "landing") {
            return (
                <div className="deck_box_supracontainer">
                    <div className={deckDone === false ? "card_container landing_deck": "card_container landing_deck_hidden"}>
                        <LoadingCard style={{display: "flex"}} />
                    </div>
                    {deck_content}
                </div>
            );
        }
    } else if (deck_content.length > 0 && props.info === undefined) {
        return (
            <div className="deck_box_supracontainer">
                {deck_content}
            </div>
        );
    }
}