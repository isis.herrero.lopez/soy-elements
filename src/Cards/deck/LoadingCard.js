import React from "react";
import "../individualCards/CardBack.css";


export default function LoadingCard(props) {
    /*when images ready, add to the div: 
    style={ props.faction === "york" ? yorkImage : lancasterImage }
    */


    return(
        <div className="card_back" style={props.style}>
            <p>Loading deck...</p>
        </div>
    )
}