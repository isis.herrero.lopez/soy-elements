import React, { useState, useEffect } from "react";
import "./ShufflingDeckBox.css";

import DeckBox from "./DeckBox";

export default function ShufflingDeckBox(props) {
    const central = 110;
    const [centralDeck, setCentralDeck] = useState([]);
    const [gralClass, setGralClass] = useState("shuffling_supracontainer waiting");

    useEffect(() => {
        if (centralDeck.length === 0) {
            const central_new_deck = [];

            for (let i = 0; i < central; i++) {
                let className;
                let delay = 0;
                let bottom;
                const side = Math.random();
                if (side < 0.5) {
                    className = "to_left";
                    const existing_left = central_new_deck.filter(item => item.className === "to_left");
                    if (existing_left.length > 0) {
                        delay = (0.002 * existing_left.length).toFixed(3);
                    }
                    bottom = existing_left.length / 2;
                } else {
                    className = "to_right";
                    const existing_right = central_new_deck.filter(item => item.className === "to_right");
                    if (existing_right.length > 0) {
                        delay = (0.002 * existing_right.length).toFixed(3);
                    }
                    bottom = existing_right.length / 2;
                }
                const card = {
                    i,
                    className,
                    delay: delay + "s",
                    bottom
                }
                central_new_deck.push(card);
            }
            setCentralDeck(central_new_deck);
        }
    }, [centralDeck.length]);

    const shuffleDeck = () => {
        setGralClass("shuffling_supracontainer");
    }

    return(
        <div className={gralClass}>
            <div>
                <DeckBox size={central} visible={props.visible} info={centralDeck} action={"shuffling"} />
            </div>
            <div className="shuffle_button" onClick={() => shuffleDeck()}>Shuffle!</div>
        </div>
    )
}