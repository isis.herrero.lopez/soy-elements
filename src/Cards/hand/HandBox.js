import React, { useState } from "react";
import "./HandBox.css";

import IndividualCardBox from "../individualCards/IndividualCardBox";

export default function HandBox(props) {
    const [visible, setVisible] = useState(props.visible);
    let hand_size;
    let hand;

    if (props.initial === true) {
        hand_size = 16;
        hand = "initialHand";
    } else {
        hand_size = 4;
        hand = "hand";
    }
    let hand_content = [];
    for (let i = 0; i < hand_size; i++) {
        hand_content.push(<IndividualCardBox 
            key={hand + (i + 1)}
            id={hand + (i + 1)}
            visible={visible}
            turning={props.turning}
            selecting={props.selecting}
        />);
    }

    const turnHand = () => {
        setVisible("front");
    }

    if (props.turning === true) {
        if (props.initial === true) {
            return(
                <div className="initial_hand_box_supracontainer turn">
                    <div>
                        {hand_content}
                    </div>
                    <div className="turn_button" onClick={() => turnHand()}>Turn!</div>
                </div>
            );
        } else {
            return(
                <div className="hand_box_supracontainer turn">
                    <div>
                        {hand_content}
                    </div>
                    <div className="turn_button" onClick={() => turnHand()}>Turn!</div>
                </div>
            );
        }
    } else {
        if (props.initial === true) {
            return(
                <div className="initial_hand_box_supracontainer">
                    {hand_content}
                </div>
            );
        } else {
            return(
                <div className="hand_box_supracontainer">
                    {hand_content}
                </div>
            );
        }
    }
}