import React from "react";
import "./CardBack.css";


export default function CardBack(props) {
    /*when images ready, add to the div: 
    style={ props.faction === "york" ? yorkImage : lancasterImage }
    */


    return(
        <div className={props.className} style={props.style}>
            <p>Card Back</p>
        </div>
    )
}