import React, { useState, useEffect } from "react";
import styled, { keyframes } from "styled-components";
import "./IndividualCardBox.css";

import CardBack from "./CardBack";
import CardFront from "./CardFront";

export default function IndividualCardBox(props) {
    const [visible, setVisible] = useState("back");
    const [turning, setTurning] = useState(false);
    const [clicked, setClicked] = useState(false);

    const deckReady = props.deckDone;
    const [StyledLeft, setStyledLeft] = useState();
    const [StyledRight, setStyledRight] = useState();
    const [StyledLanding, setStyledLanding] = useState();

    const [selected, setSelected] = useState(false);

    useEffect(() => {
        if (props.visible === "front") {
            setVisible("front");
        }
        if (props.turning === true) {
            setTurning(true);
        }

        if (props.action === "shuffling" && StyledLeft === undefined && StyledRight === undefined) {
            const toLeft = keyframes`
                0%{bottom: ${props.realBottom}px; left: 0;}
                25%{bottom: ${props.bottom}px; left: -10em;}
                26%{bottom: ${props.bottom}px; left: -10em;}
                50%{bottom: ${props.realBottom}px; left: 0;}
                51%{bottom: ${props.realBottom}px; left: 0;}
                75%{bottom: ${props.bottom}px; left: -10em;}
                76%{bottom: ${props.bottom}px; left: -10em;}
                100%{bottom: ${props.realBottom}px; left: 0;}
            `;
            const toRight = keyframes`
                0%{bottom: ${props.realBottom}px; left: 0;}
                25%{bottom: ${props.bottom}px; left: 10em;}
                26%{bottom: ${props.bottom}px; left: 10em;}
                50%{bottom: ${props.realBottom}px; left: 0;}
                51%{bottom: ${props.realBottom}px; left: 0;}
                75%{bottom: ${props.bottom}px; left: 10em;}
                76%{bottom: ${props.bottom}px; left: 10em;}
                100%{bottom: ${props.realBottom}px; left: 0;}
            `;
            const new_StyledLeft = styled.div`
                animation-delay: ${props.delay};
                animation-name: ${toLeft};
                animation-duration: 1.6s;
                animation-fill-mode: forwards;
                animation-iteration-count: 1;
                animation-timing-function: ease-in-out;
                bottom: ${props.realBottom}px;
            `;
            const new_StyledRight = styled.div`
                animation-delay: ${props.delay};
                animation-name: ${toRight};
                animation-duration: 1.6s;
                animation-fill-mode: forwards;
                animation-iteration-count: 1;
                animation-timing-function: ease-in-out;
                bottom: ${props.realBottom}px;
            `;
            setStyledLeft(new_StyledLeft);
            setStyledRight(new_StyledRight);
        }
        if (deckReady === false && StyledLeft !== undefined && StyledRight !== undefined) {
            props.handleDeck();
        }

        if (props.action === "landing" && StyledLanding === undefined) {
            const toDown = keyframes`
                0%{bottom: ${props.bottom}px;}
                100%{bottom: ${props.realBottom}px}
            `;

            const new_StyledLanding = styled.div`
                animation-delay: ${props.delay};
                animation-name: ${toDown};
                animation-duration: 0.8s;
                animation-fill-mode: forwards;
                animation-iteration-count: 1;
                animation-timing-function: ease-in-out;
                bottom: ${props.bottom}px;
            `;
            setStyledLanding(new_StyledLanding);
        }
        if (deckReady === false && StyledLanding !== undefined) {
            props.handleDeck();
        }

    }, [props, StyledLeft, StyledRight, deckReady, StyledLanding]);

    const turnCard = () => {
        if (visible === "back") {
            setVisible("front");
            setTimeout(() => (setClicked(true)), 751);
        }
    }

    const showCardUp = () => {
        setSelected(!selected);
    } 

    if (turning === false) {
        if (props.action === "shuffling" && StyledLeft !== undefined && StyledRight !== undefined) {
            if (props.className === "to_left"){
                return (
                    <StyledLeft className="card_container" id={props.id} >
                        <CardBack className="card_back" style={visible === "back" ? {display: "flex"} : {display: "none"}} />
                        <CardFront className="card_front" style={visible === "back" ? {display: "none"} : {display: "flex"}} />
                    </StyledLeft>
                );
            } else {
                return (
                    <StyledRight className="card_container" id={props.id} >
                        <CardBack className="card_back" style={visible === "back" ? {display: "flex"} : {display: "none"}} />
                        <CardFront className="card_front" style={visible === "back" ? {display: "none"} : {display: "flex"}} />
                    </StyledRight>
                );
            }
        } else if (props.action === "landing" && StyledLanding !== undefined) {
            return (
                <StyledLanding className="card_container" id={props.id}>
                    <CardBack className="card_back" style={visible === "back" ? {display: "flex"} : {display: "none"}} />
                </StyledLanding>
            );
        } else if (props.selecting === true) {
            return (
                <div className="card_container clickable" style={props.style} id={props.id} onClick={() => showCardUp()}>
                    <CardBack className="card_back" style={visible === "back" ? {display: "flex"} : {display: "none"}} />
                    <CardFront className={selected === false ? "card_front" : "card_front selected"} style={visible === "back" ? {display: "none"} : {display: "flex"}} />
                </div>
            );
        } else {
            return (
                <div className="card_container" style={props.style} id={props.id} >
                    <CardBack className="card_back" style={visible === "back" ? {display: "flex"} : {display: "none"}} />
                    <CardFront className="card_front" style={visible === "back" ? {display: "none"} : {display: "flex"}} />
                </div>
            );
        }
    } else {
        if (props.alone === true) {
            return (
                <div className={clicked === false ? "card_container clickable" : "card_container"} style={props.style} id={props.id} onClick={() => turnCard()}>
                    <CardBack className={visible === "back" ? "card_back" : "card_back_hidden"} />
                    <CardFront className={visible === "back" ? "card_front_hidden" : clicked === false ? "card_front turning" : "card_front"} />
                </div>
            );
        } else {
            return (
                <div className={clicked === false ? "card_container clickable" : "card_container"} style={props.style} id={props.id} onClick={() => turnCard()}>
                    <CardBack className={visible === "back" ? "card_back" : "card_back_hidden"} />
                    <CardFront className={visible === "back" ? "card_front_hidden" : clicked === false ? "card_front turning" : "card_front"} />
                </div>
            );
        }

    }
}