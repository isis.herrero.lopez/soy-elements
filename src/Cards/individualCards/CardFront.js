import React from "react";
import "./CardFront.css";


export default function CardFront(props) {

    return(
        <div className={props.className} style={props.style}>
            <p>Card Front</p>
        </div>
    )
}