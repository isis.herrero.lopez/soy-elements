import React, { useState } from 'react';
import './App.css';
import DiceBox from "./Dice/DiceBox"
import CardsBox from "./Cards/CardsBox";

function App() {
  const [selected, setSelected] = useState("cards");
  const [diceNumber, setDiceNumber] = useState(1);
  const [cardAction, setCardAction] = useState("cardTurning");

  if (selected === "dice") {
    return (
      <div className="App">
        <div className="dice_page">
          <div className="dice_selection">
            <p>How many dice?</p>
            <div className="dice_option" onClick={() => setDiceNumber(1)}>1</div>
            <div className="dice_option" onClick={() => setDiceNumber(2)}>2</div>
            <div className="dice_option" onClick={() => setDiceNumber(3)}>3</div>
            <div className="dice_option" onClick={() => setDiceNumber(4)}>4</div>
            <div className="dice_option" onClick={() => setDiceNumber(5)}>5</div>
          </div>
          <DiceBox quantity={diceNumber} />
        </div>
      </div>
    );
  } else if (selected === "cards") {
    return (
      <div className="App">
        <div className="card_page">
          <div className="card_selection">
            <p>Select:</p>
            <div className="card_option" onClick={() => setCardAction("showCard")}>Show card</div>
            <div className="card_option" onClick={() => setCardAction("showDeck")}>Show deck</div>
            <div className="card_option" onClick={() => setCardAction("showHand")}>Show hand</div>
            <div className="card_option" onClick={() => setCardAction("showInitialHand")}>Show initial hand</div>
            <div className="card_option" onClick={() => setCardAction("showDeploymentPosition")}>Show deployment position</div>
            <div className="card_option" onClick={() => setCardAction("cardTurning")}>Show card turning</div>
            <div className="card_option" onClick={() => setCardAction("shuffling")}>Show shuffling</div>
            <div className="card_option" onClick={() => setCardAction("landing")}>Show deck landing</div>
            <div className="card_option" onClick={() => setCardAction("handTurning")}>Show hand turning</div>
            <div className="card_option" onClick={() => setCardAction("initialHandTurning")}>Show initial hand turning</div>
            <div className="card_option" onClick={() => setCardAction("cardSelect")}>Show card selection</div>
          </div>
            <CardsBox action={cardAction} />
        </div>
      </div>
    );
  } else if (selected === "") {
    return(
    <div className="App">
      <div className="selection">
        <div className="select_option" onClick={() => setSelected("dice")}>
          <p>Dice</p>
        </div>
        <div className="select_option" onClick={() => setSelected("cards")}>
          <p>Cards</p>
        </div>
      </div>
    </div>
    );
  }
}

export default App;
