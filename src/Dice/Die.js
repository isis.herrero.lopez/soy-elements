import React, { useState, useEffect, useCallback } from 'react';
import "./Die.css";

export default function Die(props) {
    const [diceClass, setDiceClass] = useState("dice");
    const [initial, setInitial] = useState(true);
    const [frontNumber, setFrontNumber] = useState(0);
    const [fullDice, setFullDice] = useState([
        {
            number: 1,
            content: [<span key="f1_1" className="central_dot"/>],
            side: "front",
            opposite: 6
        },
        {
            number: 2,
            content: [<span key="f2_1" className="top_left_dot" />, <span key="f2_2" className="bottom_right_dot"/>],
            side: "right",
            opposite: 5
        },
        {
            number: 3,
            content: [<span key="f3_1" className="top_right_dot" />,<span key="f3_2" className="central_dot" />,<span key="f3_3" className="bottom_left_dot" />],
            side: "top",
            opposite: 4
        },
        {
            number: 4,
            content: [<span key="f4_1" className="top_left_dot" />, <span key="f4_2" className="top_right_dot" />, <span key="f4_3" className="bottom_left_dot" />, <span key="f4_4" className="bottom_right_dot"/>],
            side: "bottom",
            opposite: 3
        },
        {
            number: 5,
            content: [<span key="f5_1" className="top_left_dot" />, <span key="f5_2" className="top_right_dot" />, <span key="f5_3" className="central_dot" />, <span key="f5_4" className="bottom_left_dot" />, <span key="f5_5" className="bottom_right_dot"/>],
            side: "left",
            opposite: 2
        },
        {
            number: 6,
            content: [<span key="f6_1" className="top_left_dot" />, <span key="f6_2" className="top_right_dot" />,<span key="f6_3" className="left_central_dot" />, <span key="f6_4" className="right_central_dot" />, <span key="f6_5" className="bottom_left_dot" />, <span key="f6_6"className="bottom_right_dot"/>],
            side: "back",
            opposite: 1
        }
    ]);
    const [front, setFront] = useState([]);
    const [back, setBack] = useState([]);
    const [right, setRight] = useState([]);
    const [left, setLeft] = useState([]);
    const [top, setTop] = useState([]);
    const [bottom, setBottom] = useState([]);

    const printDots = useCallback(() => {
        const new_front = fullDice.filter(item => item.side === "front").map(item => item.content);
        setFront(new_front);
        const new_back = fullDice.filter(item => item.side === "back").map(item => item.content);
        setBack(new_back);
        const new_right = fullDice.filter(item => item.side === "right").map(item => item.content);
        setRight(new_right);
        const new_left = fullDice.filter(item => item.side === "left").map(item => item.content);
        setLeft(new_left);
        const new_top = fullDice.filter(item => item.side === "top").map(item => item.content);
        setTop(new_top);
        const new_bottom = fullDice.filter(item => item.side === "bottom").map(item => item.content);
        setBottom(new_bottom);
    }, [fullDice]);

    const newRoll = useCallback(() => {
        const new_number = Math.floor(Math.random() * (7 - 1)) + 1;
        setFrontNumber(new_number);
    }, []);

    const newFaces = useCallback(() => {
        if (frontNumber !== 0) {
            const new_opposite = fullDice[frontNumber - 1].opposite;
            let new_full_dice = [...fullDice];
            new_full_dice.map(item => item.side = "")
            new_full_dice[frontNumber - 1].side = "front";
            new_full_dice[new_opposite - 1].side = "back";
            if (frontNumber === 1) {
                new_full_dice[1].side = "right"; //2
                new_full_dice[2].side = "top"; //3
                new_full_dice[3].side = "bottom"; //4
                new_full_dice[4].side = "left"; //5
            } else if (frontNumber === 2) {
                new_full_dice[0].side = "bottom"; //1
                new_full_dice[2].side = "left"; //3
                new_full_dice[3].side = "right"; //4
                new_full_dice[5].side = "top"; //6
            } else if (frontNumber === 3) {
                new_full_dice[0].side = "right"; //1
                new_full_dice[1].side = "top"; //2
                new_full_dice[4].side = "bottom"; //5
                new_full_dice[5].side = "left"; //6
            } else if (frontNumber === 4) {
                new_full_dice[0].side = "left"; //1
                new_full_dice[1].side = "bottom"; //2
                new_full_dice[4].side = "top"; //5
                new_full_dice[5].side = "right"; //6
            } else if (frontNumber === 5) {
                new_full_dice[0].side = "top"; //1
                new_full_dice[2].side = "right"; //3
                new_full_dice[3].side = "left"; //4
                new_full_dice[5].side = "bottom"; //6
            } else if (frontNumber === 6) {
                new_full_dice[1].side = "left"; //2
                new_full_dice[2].side = "bottom"; //3
                new_full_dice[3].side = "top"; //4
                new_full_dice[4].side = "right"; //5
            }
            setFullDice(new_full_dice);
            }
    }, [frontNumber, fullDice]);
    
    useEffect(() => {
        if (initial === true && frontNumber === 0) {
            newRoll();
        }
        if (initial === true && frontNumber !== 0) {
            newFaces();
            printDots();
            setInitial(false);
            setFrontNumber(0);
        }
        if (props.rolling === true && frontNumber === 0) {
            newRoll();
        }
        if (props.rolling === true && frontNumber !== 0) {
            setDiceClass("dice add_keyframe");
            setTimeout(() => {
                newFaces();
                printDots();
            }, 1500);

            setTimeout(() => {
                props.setRolling(false);
                setDiceClass("dice");
                setFrontNumber(0);
            }, 3000);
        }

    }, [initial, printDots, newRoll, newFaces, frontNumber, props]);

    return (
        <div className="dice_container">
            <div className= {diceClass}>
                <div className="dice_side back">
                    {back}
                </div>
                <div className="dice_side front">
                    {front}
                </div>
                <div className="dice_side right">
                    {right}
                </div>
                <div className="dice_side left">
                    {left}
                </div>
                <div className="dice_side top">
                    {top}
                </div>
                <div className="dice_side bottom">
                    {bottom}
                </div>
            </div>
        </div>
    )
}