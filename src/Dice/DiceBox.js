import React, { useState } from "react";
import "./DiceBox.css";

import Die from "./Die";

export default function DiceBox(props) {
    const quantity = props.quantity;
    const  [rolling, setRolling] = useState(false);

    let dice = [];
    for (let i = 1; i <= quantity; i++) {
        dice.push(<Die key={i} rolling={rolling} setRolling={() => setRolling()}/>);
    }

    return (
        <div className="dice_box_supracontainer">
            <div className="dice_box_container">
                {dice}
            </div>
            <div className="dice_box_button" onClick={() => setRolling(true)}>
                <p>Roll dice!</p>
            </div>

        </div>
    )
}